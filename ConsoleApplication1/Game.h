#pragma once

#include <Eigen/Dense>
#include <map>
#include <vector>


using std::map;
using std::vector;
using Eigen::Matrix2cd;
using Eigen::Matrix2d;

enum GameType { G_CHSH, G_CHSH_PrisonerDilemma, G_CHSH_BattleOfSexes, G_CHSH_Chicken, G_SheriffDilemma };
enum Player { Alice = 0, Bob = 1 };

struct Type {
	int alice;
	int bob;
};

struct Answer {
	int alice;
	int bob;
};

class CHSH;
class CHSH_BattleOfSexes;
class CHSH_Chicken;
class CHSH_PrisonerDilemma;
class SheriffDilemma;

class Game {
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	static Game* GetGame(GameType);
	double GetUtility(Player, Type, Answer);
	double GetTypeProbability(Type);
	size_t GetPlayerTypeCount(Player);
	size_t GetPlayerAnswerCount(Player);

protected:
	Game();
	virtual ~Game();

	map<Player, size_t> playerTypeCounts;
	map<Player, size_t> playerAnswerCounts;

	double** typeProbabilities;

private:
	virtual Matrix2d* GetBimatrix(Type) = 0;
	static CHSH* chsh;
	static CHSH_BattleOfSexes* chsh_bos;
	static CHSH_Chicken* chsh_chicken;
	static CHSH_PrisonerDilemma* chsh_pd;
	static SheriffDilemma* sd;
};

class CHSH : public Game {
public:
	CHSH();

private:
	Matrix2d* GetBimatrix(Type);
	Matrix2d matrices[2][2];
};

class CHSH_BattleOfSexes : public Game {
public:
	CHSH_BattleOfSexes();

private:
	Matrix2d* GetBimatrix(Type);
	Matrix2d matrices[2][2];
};

class CHSH_Chicken : public Game {
public:
	CHSH_Chicken();

private:
	Matrix2d* GetBimatrix(Type);
	Matrix2d matrices[2][2];
};

class CHSH_PrisonerDilemma : public Game {
public:
	CHSH_PrisonerDilemma();

private:
	Matrix2d* GetBimatrix(Type);
	Matrix2d matrices[2][2];
};

class SheriffDilemma : public Game {
public:
	SheriffDilemma();

private:
	Matrix2d* GetBimatrix(Type);
	Matrix2d matrices[2][2];
};