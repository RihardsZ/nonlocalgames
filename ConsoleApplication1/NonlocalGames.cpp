#include <chrono>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <Eigen/Dense>
#include <unsupported/Eigen/KroneckerProduct>

#include "Game.h"
#include "ProjectionGenerator.h"


using Eigen::Matrix2cd;
using Eigen::Matrix4cd;
using Eigen::Vector2cd;
using Eigen::Vector4cd;

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

using std::max;

struct utility_t 
{
	double aliceUtility;
	double bobUtility;

	utility_t operator+(utility_t other) {
		return { other.aliceUtility + aliceUtility, other.bobUtility + bobUtility };
	}

	utility_t operator*(double multiplier) {
		return{ aliceUtility * multiplier, bobUtility * multiplier };
	}

	double average()
	{
		return (aliceUtility + bobUtility) / 2.0;
	}
};

struct history_t 
{
	vector< vector<Matrix2cd> > aliceMatrices;
	vector< vector<Matrix2cd> > bobMatrices;

	string message;
	utility_t utility;

	size_t numAliceTypes;
	size_t numBobTypes;

	size_t numAliceAnswers;
	size_t numBobAnswers;

	history_t(Game* game) :
		aliceMatrices(game->GetPlayerTypeCount(Alice), vector<Matrix2cd>(game->GetPlayerAnswerCount(Alice))),
		bobMatrices(game->GetPlayerTypeCount(Bob), vector<Matrix2cd>(game->GetPlayerAnswerCount(Bob)))
	{
		numAliceTypes = game->GetPlayerTypeCount(Alice);
		numBobTypes = game->GetPlayerTypeCount(Bob);

		numAliceAnswers = game->GetPlayerAnswerCount(Alice);
		numBobAnswers = game->GetPlayerAnswerCount(Bob);
	}
};

std::ostream &operator<<(std::ostream &os, const history_t &history) {
	os << "-------------History start-------------" << endl;
	os << "Message: " << history.message << endl;
	os << "Utility: Alice " << history.utility.aliceUtility << ", Bob " << history.utility.bobUtility << endl;

	for (int type = 0; type < max(history.numAliceTypes, history.numBobTypes); ++type) {
		for (int answer = 0; answer <= max(history.numAliceAnswers, history.numBobAnswers); ++answer) {

			if (
				type < history.numAliceTypes 
				&& answer < history.numAliceAnswers
			) {
				os << "Alice's matrix for t_A = " << type << " and a_A = " << answer << endl;
				os << history.aliceMatrices[type][answer] << endl << endl;
			}

			if (
				type < history.numBobTypes
				&& answer < history.numBobAnswers
			) {
				os << "Bob's matrix for t_B = " << type << " and a_B = " << answer << endl;
				os << history.bobMatrices[type][answer] << endl << endl;
			}
		}
	}

	return os;

}

class NonlocalGames 
{
private:
	Vector4cd psi;
	Game * game;
	ProjectionGenerator * projectionGenerator;
	vector<history_t> history;
public:
	
	NonlocalGames(GameType gameType, ProjectionStrategy projectionStrategy)
	{
		game = Game::GetGame(gameType);
		projectionGenerator = ProjectionGenerator::GetProjectionGenerator(projectionStrategy);
		psi = GetEntangledState();
	}

	void SaveHistory(string message, utility_t utility)
	{
		history_t record(game);
		for (int type = 0; type < max(record.numAliceTypes, record.numBobTypes); ++type) {
			for (int answer = 0; answer < max(record.numAliceAnswers, record.numBobAnswers); ++answer) {

				if (
					type < record.numAliceTypes
					&& answer < record.numAliceAnswers
				) {
					record.aliceMatrices[type][answer] = projectionGenerator->GetProjection(Alice, type, answer);
				}

				if (
					type < record.numBobTypes
					&& answer < record.numBobAnswers
				) {
					record.bobMatrices[type][answer] = projectionGenerator->GetProjection(Bob, type, answer);
				}

			}
		}

		record.message = message;
		record.utility = utility;

		history.push_back(record);
		
	}

	void PrintHistory()
	{
		cout << "In total, " << history.size() << endl;
		cout << history.back();
	}

	void reseed(Player p)
	{
		projectionGenerator->reseed(p);
	}

	Vector4cd GetEntangledState()
	{
		Vector4cd ent(1, 0, 0, 1);
		//Vector4cd ent(0, 1, 1, 0);

		return ent / sqrt(2);
	}

	utility_t GetUtility(const Vector4cd &psi, vector<Matrix2cd> aliceMats, vector<Matrix2cd> bobMats, int t_A, int t_B)
	{
		Matrix4cd matrix;
		complex<double> prob;
		utility_t utility = { .0, .0 };

		for (int a_A = 0; a_A < game->GetPlayerAnswerCount(Alice); ++a_A) {
			for (int a_B = 0; a_B < game->GetPlayerAnswerCount(Bob); ++a_B) {

				matrix = Eigen::kroneckerProduct(
					aliceMats[a_A],
					bobMats[a_B]
				);

				prob = psi.adjoint() * (matrix * psi);
				utility.aliceUtility += game->GetUtility(Alice, {t_A, t_B}, {a_A, a_B}) * prob.real();
				utility.bobUtility += game->GetUtility(Bob, {t_A, t_B}, {a_A, a_B}) * prob.real();

			}
		}

		return utility;
	}

	utility_t GetGameValues()
	{
		vector< vector<Matrix2cd> > aliceMatrices(game->GetPlayerTypeCount(Alice), vector<Matrix2cd>(game->GetPlayerAnswerCount(Alice)));
		vector< vector<Matrix2cd> > bobMatrices(game->GetPlayerTypeCount(Bob), vector<Matrix2cd>(game->GetPlayerAnswerCount(Bob)));

		for (int type = 0; type < game->GetPlayerTypeCount(Alice); ++type) {
			for (int action = 0; action < game->GetPlayerAnswerCount(Alice); ++action) {
				aliceMatrices[type][action] = projectionGenerator->GetProjection(Alice, type, action);
			}
		}

		for (int type = 0; type < game->GetPlayerTypeCount(Bob); ++type) {
			for (int action = 0; action < game->GetPlayerAnswerCount(Bob); ++action) {
				bobMatrices[type][action] = projectionGenerator->GetProjection(Bob, type, action);
			}
		}

		utility_t utility = { .0, .0 }, temp;

		for (int t_A = 0; t_A < game->GetPlayerTypeCount(Alice); ++t_A) {
			for (int t_B = 0; t_B < game->GetPlayerTypeCount(Bob); ++t_B) {
				temp = GetUtility(psi, aliceMatrices[t_A], bobMatrices[t_B], t_A, t_B);
				utility = utility + temp * game->GetTypeProbability({ t_A, t_B });
			}
		}

		return utility;
	}
};

int main()
{	
	utility_t utility;
	utility_t maxIndividual = {-10000.0, -10000.0};
	utility_t maxAvg = { -10000.0, -10000.0 };

	GameType gameTypes[] = { G_CHSH, G_CHSH_BattleOfSexes, G_CHSH_PrisonerDilemma, G_CHSH_Chicken, G_SheriffDilemma };
	ProjectionStrategy projectionStrategies[] = { PS_RANDOM, PS_CHSH };

	int gameId, stratId, iterations = 1;

	cout << "Pick the type of a game:" << endl;
	cout << "\t0 - Cooperative CHSH game" << endl;
	cout << "\t1 - CHSH / Battle of Sexes game" << endl;
	cout << "\t2 - CHSH / Prisoner's Dilemma game" << endl;
	cout << "\t3 - CHSH / Chicken game" << endl;
	cout << "\t4 - Sheriff's dilemma game" << endl;

	cout << "Anything other exits program" << endl;

	cin >> gameId;

	if (gameId < 0 || gameId > 4) {
		cout << "Invalid input." << endl;
		exit(EXIT_FAILURE);
	}

	cout << "Pick the projection generation strategy: " << endl;
	cout << "\t0 - Random projections" << endl;
	cout << "\t1 - CHSH projections (assumes just a single run)" << endl;

	cout << "Anything other exits program" << endl;

	cin >> stratId;

	if (stratId < 0 || stratId > 1) {
		cout << "Invalid input." << endl;
		exit(EXIT_FAILURE);
	}

	if (stratId == 0) {
		cout << "Pick the number of iterations (a positive integer): " << endl;
		cin >> iterations;
	}

	if (iterations < 1) {
		cout << "Invalid input." << endl;
		exit(EXIT_FAILURE);
	}


	//NonlocalGames ng(/*G_CHSH, */G_CHSH_PrisonerDilemma/*, G_CHSH_BattleOfSexes, G_CHSH_Chicken, G_SheriffDilemma*/, PS_RANDOM/*PS_CHSH*/);
	NonlocalGames ng(gameTypes[gameId], projectionStrategies[stratId]);
	cout << "Please wait..." << endl;

	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	for (int i = 0; i < iterations; ++i) {
		utility = ng.GetGameValues();

		if (utility.aliceUtility > maxIndividual.aliceUtility) {
			maxIndividual.aliceUtility = utility.aliceUtility;
		}

		if (utility.bobUtility > maxIndividual.bobUtility) {
			maxIndividual.bobUtility = utility.bobUtility;
		}

		if (utility.average() > maxAvg.average()) {
			maxAvg = utility;
			ng.SaveHistory("max average", utility);
		}

		ng.reseed(Alice);
		ng.reseed(Bob);
	}

	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	long long duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	cout << "Done." << endl;

	cout << "Max individual utilities in " << iterations << " iterations: Alice wins " << maxIndividual.aliceUtility << ", Bob wins " << maxIndividual.bobUtility << endl;

	cout << "Max total/average utilities in " << iterations << " iterations: Alice wins " << maxAvg.aliceUtility << ", Bob wins " << maxAvg.bobUtility << endl;

	cout << "Execution took " << duration << "us" << endl;

	cout << "Histories saved: " << endl;
	ng.PrintHistory();

	char pause;
	cin >> pause;
}
