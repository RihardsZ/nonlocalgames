#include "Game.h"

Game * Game::GetGame(GameType type)
{
	switch (type) {
	case G_CHSH:
		return Game::chsh;
		break;

	case G_CHSH_BattleOfSexes:
		return Game::chsh_bos;
		break;

	case G_CHSH_Chicken:
		return Game::chsh_chicken;
		break;

	case G_CHSH_PrisonerDilemma:
		return Game::chsh_pd;
		break;

	case G_SheriffDilemma:
		return Game::sd;
		break;

	default:
		throw new std::exception("Unknown game type");
	}
}

double Game::GetUtility(Player player, Type type, Answer answer)
{
	return GetBimatrix(type)[player](answer.alice, answer.bob);
}

double Game::GetTypeProbability(Type type)
{
	return typeProbabilities[type.alice][type.alice];
}

size_t Game::GetPlayerTypeCount(Player player)
{
	auto typeCount = playerTypeCounts.find(player);

	if (typeCount == playerTypeCounts.end()) {
		throw new std::exception("No type count defined for that player!");
	}

	return typeCount->second;
}

size_t Game::GetPlayerAnswerCount(Player player)
{
	auto answerCount = playerAnswerCounts.find(player);

	if (answerCount == playerAnswerCounts.end()) {
		throw new std::exception("No answer count defined for that player!");
	}

	return answerCount->second;
}

Game::Game() {}
Game::~Game() {}
CHSH* Game::chsh = new CHSH();
CHSH_BattleOfSexes* Game::chsh_bos = new CHSH_BattleOfSexes();
CHSH_Chicken* Game::chsh_chicken = new CHSH_Chicken();
CHSH_PrisonerDilemma* Game::chsh_pd = new CHSH_PrisonerDilemma();
SheriffDilemma* Game::sd = new SheriffDilemma();


CHSH::CHSH()
{
	// by subgame matrix, then by player, i.e.
	//				Alice	Bob
	// subgame 0
	// subgame 1

	// i.e. subgame 0, Alice
	matrices[0][Alice] <<	1.0, 0.0,
							0.0, 1.0;
	// i.e. subgame 0, Bob
	matrices[0][Bob] <<		1.0, 0.0,
							0.0, 1.0;
	// etc.
	matrices[1][Alice] <<	0.0, 1.0,
							1.0, 0.0;

	matrices[1][Bob] <<		0.0, 1.0,
							1.0, 0.0;

	playerTypeCounts.insert(std::make_pair(Alice, 2));
	playerTypeCounts.insert(std::make_pair(Bob, 2));

	playerAnswerCounts.insert(std::make_pair(Alice, 2));
	playerAnswerCounts.insert(std::make_pair(Bob, 2));


	typeProbabilities = new double*[2];
	typeProbabilities[0] = new double[2];
	typeProbabilities[1] = new double[2];

	typeProbabilities[0][0] = typeProbabilities[0][1] = typeProbabilities[1][0] = typeProbabilities[1][1] = 0.25;

	//to do delete[] in destructor

}

Matrix2d* CHSH::GetBimatrix(Type type)
{
	return matrices[type.alice & type.bob];
}

CHSH_BattleOfSexes::CHSH_BattleOfSexes()
{
	matrices[0][Alice] <<	1.0, 0.0,
							0.0, 0.5;

	matrices[0][Bob] <<		0.5, 0.0,
							0.0, 1.0;

	matrices[1][Alice] <<	0.0, 0.75,
							0.75, 0.0;

	matrices[1][Bob] <<		0.0, 0.75,
							0.75, 0.0;

	playerTypeCounts.insert(std::make_pair(Alice, 2));
	playerTypeCounts.insert(std::make_pair(Bob, 2));

	playerAnswerCounts.insert(std::make_pair(Alice, 2));
	playerAnswerCounts.insert(std::make_pair(Bob, 2));

	typeProbabilities = new double*[2];
	typeProbabilities[0] = new double[2];
	typeProbabilities[1] = new double[2];

	typeProbabilities[0][0] = typeProbabilities[0][1] = typeProbabilities[1][0] = typeProbabilities[1][1] = 0.25;

	//to do delete[] in destructor
}

Matrix2d* CHSH_BattleOfSexes::GetBimatrix(Type type)
{
	return matrices[type.alice & type.bob];
}

CHSH_Chicken::CHSH_Chicken()
{
	matrices[0][Alice] <<	0.0, -0.1,
							0.1, -1.0;

	matrices[0][Bob] <<		0.0, 0.1,
							-0.1, -1.0;

	matrices[1][Alice] <<	0.0, 0.75,
							0.75, 0.0;

	matrices[1][Bob] <<		0.0, 0.75,
							0.75, 0.0;

	playerTypeCounts.insert(std::make_pair(Alice, 2));
	playerTypeCounts.insert(std::make_pair(Bob, 2));

	playerAnswerCounts.insert(std::make_pair(Alice, 2));
	playerAnswerCounts.insert(std::make_pair(Bob, 2));

	typeProbabilities = new double*[2];
	typeProbabilities[0] = new double[2];
	typeProbabilities[1] = new double[2];

	typeProbabilities[0][0] = typeProbabilities[0][1] = typeProbabilities[1][0] = typeProbabilities[1][1] = 0.25;

	//to do delete[] in destructor
}

Matrix2d* CHSH_Chicken::GetBimatrix(Type type)
{
	return matrices[type.alice & type.bob];
}

CHSH_PrisonerDilemma::CHSH_PrisonerDilemma()
{
	matrices[0][Alice] <<	-1.0/3, -1.0,
							0.0, -2.0/3;

	matrices[0][Bob] <<		-1.0/3, 0.0,
							-1.0, -2.0/3;

	matrices[1][Alice] <<	0.0, 0.75,
							0.75, 0.0;

	matrices[1][Bob] <<		0.0, 0.75,
							0.75, 0.0;

	playerTypeCounts.insert(std::make_pair(Alice, 2));
	playerTypeCounts.insert(std::make_pair(Bob, 2));

	playerAnswerCounts.insert(std::make_pair(Alice, 2));
	playerAnswerCounts.insert(std::make_pair(Bob, 2));

	typeProbabilities = new double*[2];
	typeProbabilities[0] = new double[2];
	typeProbabilities[1] = new double[2];

	typeProbabilities[0][0] = typeProbabilities[0][1] = typeProbabilities[1][0] = typeProbabilities[1][1] = 0.25;

	//to do delete[] in destructor
}

Matrix2d* CHSH_PrisonerDilemma::GetBimatrix(Type type)
{
	return matrices[type.alice & type.bob];
}

SheriffDilemma::SheriffDilemma()
{
	matrices[0][Alice] << -3.0, -1.0,
						-2.0, 0.0;

	matrices[0][Bob] << -1.0, -2.0,
						-1.0, 0.0;

	matrices[1][Alice] << 0.0, 2.0,
						-2.0, -1.0;

	matrices[1][Bob] << 0.0, -2.0,
						-1.0, 1.0;

	playerTypeCounts.insert(std::make_pair(Alice, 2));
	playerTypeCounts.insert(std::make_pair(Bob, 2));

	playerAnswerCounts.insert(std::make_pair(Alice, 2));
	playerAnswerCounts.insert(std::make_pair(Bob, 2));

	typeProbabilities = new double*[2];
	typeProbabilities[0] = new double[2];
	typeProbabilities[1] = new double[2];

	typeProbabilities[0][0] = typeProbabilities[0][1] = typeProbabilities[1][0] = typeProbabilities[1][1] = 0.25;

	//to do delete[] in destructor
}

Matrix2d* SheriffDilemma::GetBimatrix(Type type)
{
	return matrices[type.alice & type.bob];
}
