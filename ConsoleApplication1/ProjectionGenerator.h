#pragma once

// for random seeding
#include <complex>
#include <chrono>
#include <random>

#include <Eigen/Dense>

#include "Game.h"

using std::complex;
using std::default_random_engine;
using std::uniform_real_distribution;
using Eigen::Vector2cd;

// for complex inputs a la 1i for imaginary unit
using namespace std::complex_literals;

typedef std::chrono::high_resolution_clock hr_clock;

enum ProjectionStrategy {PS_RANDOM, PS_CHSH};

class ProjectionGenerator {
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	ProjectionGenerator();
	static ProjectionGenerator * GetProjectionGenerator(ProjectionStrategy);
	virtual Matrix2cd GetProjection(Player, int, int) = 0;
	virtual ~ProjectionGenerator();

	void reseed(Player);

protected:
	static Matrix2cd MakeProjection(int, double);
	// as in [Cleve et al. 2010]
	static Vector2cd phi0(double angle);
	static Vector2cd phi1(double angle);
	static Matrix2cd parameterizedUnitary(double, double, double, double);

private:
	virtual void reset(Player) = 0;
};



class RandomProjectionGenerator : public ProjectionGenerator
{
private:
	double GetRandomAngle();
	hr_clock::duration now;
	default_random_engine generator;
	Matrix2cd randomUnitary();
	void reset(Player);

	uniform_real_distribution<double> zeroTo2PI;
	uniform_real_distribution<double> zeroToOne;
	// By player, then by type
	Matrix2d randomAngles;

public:
	RandomProjectionGenerator();
	Matrix2cd GetProjection(Player, int, int);
};



class ChshProjectionGenerator : public ProjectionGenerator
{
private:
	Matrix2cd GetAliceObservable(int t, int a);
	Matrix2cd GetBobObservable(int t, int a);
	void reset(Player);

public:
	Matrix2cd GetProjection(Player, int, int);
};
