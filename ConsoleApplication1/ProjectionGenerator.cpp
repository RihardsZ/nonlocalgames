#include "ProjectionGenerator.h"

ProjectionGenerator::ProjectionGenerator() {}
ProjectionGenerator::~ProjectionGenerator() {}

ProjectionGenerator * ProjectionGenerator::GetProjectionGenerator(ProjectionStrategy projectionStrategy)
{
	switch (projectionStrategy) {
	case PS_CHSH:
		return new ChshProjectionGenerator();
	case PS_RANDOM:
		return new RandomProjectionGenerator();
	default:
		throw new std::exception("Unknown projection strategy!");
	}
}

void ProjectionGenerator::reseed(Player p)
{
	reset(p);
}

/**
* Answer = 0 | 1
*/
Matrix2cd ProjectionGenerator::MakeProjection(int answer, double angleRad)
{
	double finalAngle = answer ? (M_PI / 2 + angleRad) : angleRad;
	Vector2cd temp(cos(finalAngle), sin(finalAngle));

	return temp * temp.adjoint();
}


Vector2cd ProjectionGenerator::phi0(double angle)
{
	Vector2cd phi(cos(angle), sin(angle));

	return phi;
}

Vector2cd ProjectionGenerator::phi1(double angle)
{
	Vector2cd phi(-sin(angle), cos(angle));

	return phi;
}

Matrix2cd ProjectionGenerator::parameterizedUnitary(double alpha, double phi, double psi, double chi)
{
	complex<double> multiplier = exp(1i * alpha);

	Matrix2cd matrix;

	matrix(0, 0) = exp(1i * psi) * cos(phi);
	matrix(0, 1) = exp(1i * chi) * sin(phi);
	matrix(1, 0) = -exp(-1i * chi) * sin(phi);
	matrix(1, 1) = exp(-1i * psi) * cos(phi);

	return multiplier * matrix;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

RandomProjectionGenerator::RandomProjectionGenerator()
{
	now = hr_clock::now().time_since_epoch();
	generator = default_random_engine(now.count());

	zeroTo2PI = uniform_real_distribution<double>(0.0, 2 * M_PI);
	zeroToOne = uniform_real_distribution<double>(0.0, 1);

	reset(Alice);
	reset(Bob);
}

void RandomProjectionGenerator::reset(Player p)
{
	randomAngles(p, 0) = GetRandomAngle();
	randomAngles(p, 1) = GetRandomAngle();
}

/**
*	Uses method described by Maris Ozols in paper "How to generate a random unitary matrix", 2009
*/


// check of unitarity
/*Matrix2cd randomU = randomUnitary();

cout << "Random unitary matrix U: " << endl;
cout << randomU << endl;

cout << "Check if U* U = I : " << endl;
cout << randomU.adjoint() * randomU << endl;*/
Matrix2cd RandomProjectionGenerator::randomUnitary()
{
	Matrix2cd matrix;
	double alpha, psi, chi, xi, phi;

	alpha = zeroTo2PI(generator);
	psi = zeroTo2PI(generator);
	chi = zeroTo2PI(generator);

	xi = zeroToOne(generator);

	phi = asin(sqrt(xi));

	return parameterizedUnitary(alpha, phi, psi, chi);
}

Matrix2cd RandomProjectionGenerator::GetProjection(Player player, int type, int answer)
{
	return MakeProjection(answer, randomAngles(player, type));
}

/**
* returns angle in boundaries 0..2PI
*/
double RandomProjectionGenerator::GetRandomAngle()
{
	return zeroTo2PI(generator);
}

////////////////////////////////////////////////////////////////////////////////////////////

void ChshProjectionGenerator::reset(Player p)
{
	// do nothing
}

Matrix2cd ChshProjectionGenerator::GetProjection(Player player, int type, int answer)
{
	switch (player) {
	case Alice:
		return GetAliceObservable(type, answer);
		break;

	case Bob:
		return GetBobObservable(type, answer);
		break;

	default:
		throw new std::exception("Unknown player!");
	}
}

Matrix2cd ChshProjectionGenerator::GetAliceObservable(int t, int a)
{
	Matrix2cd aliceObs;
	double arg;
	Vector2cd(*phi) (double);

	if (t == 0) {
		arg = 0;
	} else if (t == 1) {
		arg = M_PI / 4.0;
	} else {
		throw std::exception("Unknown type for Alice!");
	}

	if (a == 0) {
		phi = phi0;
	} else if (a == 1) {
		phi = phi1;
	} else {
		throw std::exception("Unknown answer for Alice!");
	}

	Vector2cd temp = phi(arg);

	aliceObs = temp * temp.adjoint();

	//cout << "GetAliceObservable, matrix = " << endl << aliceObs << endl;

	return aliceObs;
}

Matrix2cd ChshProjectionGenerator::GetBobObservable(int t, int a)
{
	Matrix2cd bobObs;
	double arg;
	Vector2cd(*phi) (double);

	if (t == 0) {
		arg = M_PI / 8.0;
	} else if (t == 1) {
		arg = -M_PI / 8.0;
	} else {
		throw std::exception("Unknown type for Bob!");
	}

	if (a == 0) {
		phi = phi0;
	} else if (a == 1) {
		phi = phi1;
	} else {
		throw std::exception("Unknown answer for Bob!");
	}

	Vector2cd temp = phi(arg);

	bobObs = temp * temp.adjoint();

	return bobObs;
}
